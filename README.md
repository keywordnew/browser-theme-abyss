# Abyss

A theme inspired by VSCode's Abyss.

2021.08.10: [Firefox 91.0](https://www.mozilla.org/en-US/firefox/91.0/releasenotes/) now _automatically enables High Contrast Mode when "Increase Contrast" is checked on MacOS._ This overrides the background tab colour, making it bright. To allow the dark background color specified by the theme to return:

![](contrast-fix.gif)

## History

* v1.0: seeded using [Firefox Color](https://color.firefox.com/). It has limitations, for example, that don't allow setting of the new tab page background. 
* v1.2: Handful of changes and updates, to sidebar and new tab background colors.

## Preparing the .zip file

You'll need this file for uploading and testing this theme.

To create the file, put `manifest.json` into a .zip file. In MacOS, you can right-click the file and select "Compress".